Hello informatica!

Please insure that you have GIT installed
```
sudo apt-get install git
```

Please create a new directory named flask

```
sudo mkdir ~/flask
```

Enter to the folder
```
cd ~/flask
```

clone the flask project
```
sudo git clone https://github.com/prakhar1989/docker-curriculum.git
```

Enter to the folder docker-curriculum and remove all docker files
```
cd docker-curriculum/flask-app && sudo rm -f Docker* 
```

Please create the Dockerfile file
```
sudo nano Dockerfile
```

Please enter this code to the Dockerfile
```
# our base image
FROM python:3-onbuild

# specify the port number the container should expose
EXPOSE 5000

# run the application
CMD ["python", "./app.py"]
```

Please build the docker file
```
docker build -t a/suprise .
```

Please run the application from the image nodewebapp:v1
```
docker run -d -p 8888:5000 a/suprise
```
Enter the text below to your browser
```
http://<Your-IP>:8888
```

Or try via comandline's curl command
```
Curl http://<Your-IP>:8888
```

Cleanup the environment
```
docker rm  $(docker ps -a -q)
docker rmi $(docker images -a -q)
```